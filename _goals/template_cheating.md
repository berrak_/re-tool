---
type: goal
acronym: cheating
isTemplate: true
author: 
    - sbe
title: Keep the students from cheating
goalText: >
    A system like Divekit should be designed in such a way that students are actively discouraged from cheating.
sources:
    - reference: [interview, fkr_interview, "Minute 00-23-21"]
    - reference: [interview, nnprof1_interview, "Minute 00-56-10"]
history:
    v1:
        date: 2021-07-02
        comment: initially created
    v2:
        date: 2021-07-02
        comment: added source and reference to sub-goals
todo: 
ignore:
    - w094
---

## Reasoning

Cheating and deception occur at universities, which are often regarded as places of high integrity. and probity due 
This applies in particular to examinations. After all, a lot depends on students successfully passing exams 
during their studies: from being able to finish their studies at all to having better chances of being hired 
in their professional life. 

An educational system like Divekit needs to discourage students from cheating. 
